package ru.rks.Rational;

/**
 * @author Рязанов К.С. 15ОИТ18
 */
public class Rational {
    private int num;
    private int denum;

    public Rational(int numerator, int denominator) {
        this.num = numerator;
        this.denum = denominator;
    }

    public Rational() {
        this(1, 1);
    }

    public int getNum() {
        return num;
    }

    public int getDenum() {
        return denum;
    }

    public Rational(int num) {
        this.num = num;
        this.denum = 1;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    @Override
    public String toString() {
        if (num == denum) {
            return "1";
        }
        if (denum == 1) {
            return num + "";
        }
        return num + "/" + denum;
    }

    /**
     * Метод выполняет сложение дробей.
     *
     * @param rational
     * @return Результат сложения
     */
    public Rational addition(Rational rational) {
        if (this.denum != rational.denum) {
            Rational tmp = new Rational(this.num * rational.denum + rational.num * this.denum, this.denum * rational.denum);
            tmp.reduction();
            return tmp;
        } else {
            Rational tmp = new Rational(this.num * rational.denum + rational.num * this.denum, this.denum * rational.denum);
            return tmp;
        }
    }

    /**
     * Метод для вычисления разности дробей
     *
     * @param rational
     * @return Результат вычитания
     */
    public Rational difference(Rational rational) {
        if (this.denum != rational.denum) {
            return new Rational(this.num * rational.denum - rational.num * this.denum, this.denum * rational.denum);
        } else {
            Rational tmp = new Rational(this.num - rational.num, this.denum);
            tmp.reduction();
            return tmp;
        }
    }

    /**
     * Метод для вычисления произведения дробей
     *
     * @param rational
     * @return Результат умножения
     */

    public Rational multiplication(Rational rational) {
        Rational tmp = new Rational(this.num * rational.num, this.denum * rational.denum);
        tmp.reduction();
        return tmp;
    }


    /**
     * Метод для вычисления деления  дробей
     *
     * @param rational
     * @return Резултат деления
     */
    public Rational division(Rational rational) {
        Rational tmp = new Rational(this.num * rational.denum, this.denum * rational.num);
        tmp.reduction();
        return tmp;
    }

    /**
     * Метод для сокращения дроби
     */
    public void reduction() {
        while ((num % 2 != 0 && denum % 2 != 0) && (num % 3 != 0 & denum % 3 != 0) && (num % 5 != 0 && denum % 5 != 0)) {
            if (num % 5 == 0 && denum % 5 == 0) {
                num /= 5;
                denum /= 5;
            }
            if (num % 3 == 0 && denum % 3 == 0) {
                num /= 3;
                denum /= 3;
            }
            if (num % 2 == 0 && denum % 2 == 0) {
                denum /= 2;
                num /= 2;
            }
        }
    }

}
