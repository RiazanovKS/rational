package ru.rks.Rational;

import java.io.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс считывает данные из файла Input.txt , выполняет арифметические операции и записывает результаты в файл Output.txt .
 *
 * @author Рязанов К.С. 15ОИТ18
 */
public class Main {
    public static void main(String[] args) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("Input.txt"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Output.txt"));
             BufferedWriter eror = new BufferedWriter(new FileWriter("eror.txt"))) {
            String string;
            Rational rational1 = new Rational();
            Rational rational2 = new Rational();
            while ((string = bufferedReader.readLine()) != null) {
                if (!isCheckup(string)) {
                    eror.write(string + "\n");
                } else {
                    String[] strings = string.split(" ");
                    split(0, strings, rational1);
                    split(2, strings, rational2);


                    switch (strings[1]) {
                        case "+":
                            bufferedWriter.write(rational1.addition(rational2) + "\n");
                            break;
                        case "-":
                            bufferedWriter.write("" + rational1.difference(rational2) + "\n");
                            break;
                        case "/":
                            bufferedWriter.write("" + rational1.division(rational2) + "\n");
                            break;
                        case "*":
                            bufferedWriter.write("" + rational1.multiplication(rational2) + "\n");
                            break;
                    }

                }
            }
        }
    }

    /**
     * Метод разбивает части строк по слешу и преобразуя в целочисленный тип , помещает в объект Ratioanl.
     *
     * @param n
     * @param string
     * @param rational
     */
    public static void split(int n, String[] string, Rational rational) {
        String[] mas = new String[2];
        for (int i = 0; i < string[n].split("/").length; i++) {
            savearray(i, mas, string[n].split("/")[i]);
        }
        rational.setNum(Integer.parseInt(mas[0]));
        rational.setDenum(Integer.parseInt(mas[1]));
    }

    /**
     * Метод возвращает булевое значение в зависимости от валидности строки
     *
     * @param string
     * @return true- если строка валидная, false - если невалидная.
     */

    private static boolean isCheckup(String string) {
        Pattern pattern = Pattern.compile("^-?[1-9][0-9]*/[1-9][0-9]*\\s[*+-/]\\s-?[1-9][0-9]*/[1-9][0-9]*$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    /**
     * Метод для сохранения строки в массив.
     *
     * @param i
     * @param mas
     * @param retval
     */
    public static void savearray(int i, String[] mas, String retval) {
        mas[i] = retval;
    }
}


